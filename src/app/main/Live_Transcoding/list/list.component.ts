import { Component, HostListener, OnInit } from '@angular/core';
import { ActivationEnd, Router } from '@angular/router';
import { map, timer } from 'rxjs';
import { FetchService } from 'src/app/services/fetch.service';
import Swal from 'sweetalert2';
import * as Clappr from 'clappr';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss'],
})
export class ListComponent implements OnInit {
  token = localStorage.getItem('token');
  liveList: any = [];
  changeLiveList:any[] = [];
  filterStatus = 0;
  status: any = [];
  hello: any = [];
  image: any = [];
  new: any = {};
  list: any;
  names: string;
  search: any;
  serverList:any = [];
  selectedServer:string;
  job_ids:any[]=[];
  ticker_text;
  CPlayer:any;
  is_thumb_opened = false;
  notificationsList:any[] = [];
  selectedNotifyList:any={};
  count=0;
  pages:any[]=[];
  offset=0;
  filterActions = [
    // {id:0,name:'inactive'},
    // {id:1,name:'active'},
    {id:0,name:'all'},
    {id:2,name:'inprocess'},
    {id:3,name:'completed'},
    {id:4,name:'failed'},
    {id:5,name:'started'},
    {id:6,name:'stopped'},
    // {id:7,name:'notification'},
    {id:8,name:'notification'},
  ]
  constructor(private fd: FetchService, private router: Router) { }

  ngOnInit(): void {
    this.TranscodingData();
  }
  TranscodingData() {
    // this.offset = this.offset + 10;
    this.fd.getTranscoding(this.token, this.offset, this.filterStatus).pipe(map((data) => {
      console.log(data, 'poooooo')
      this.liveList = data.result;
      this.count = data.count;
      let pageCount = Math.ceil(this.count/10);
      let pages:any[]=[];
      for(let i=0;i<pageCount;i++){
        // console.log(i, 'ioii')
        pages.push({active:false});
        // this.pages.map(res=>i==0?res.active=true:res.active);
      }
      this.pages = pages;
      let pageIndx = this.offset/10;
      this.pages[pageIndx]['active']=true;
      this.liveList.map(res=>{
        res.server_name = '';
        res.is_selected = false;
      });
      let listcheck_all: any = document.getElementById('listcheck_all');
      let selectedAction: any = document.getElementById('selectedAction');
      selectedAction.innerHTML='';
      selectedAction.innerHTML = this.addingSelection;

      listcheck_all.checked = false;
      this.getServerList();

      // console.log(this.liveList);
      this.status = data.result.map((item) => {
        return item.status;
      });
      this.hello = [];
      this.image = [];

      for (let i in this.status) {
        if (this.status[i] == 0) {
          this.hello.push('Inactive');
        } else if (this.status[i] == 1) {
          this.hello.push('Active');
        } else if (this.status[i] == 2) {
          this.hello.push('In-process');
        } else if (this.status[i] == 3) {
          this.hello.push('Completed');
        } else if (this.status[i] == 4) {
          this.hello.push('Failed');
        } else if (this.status[i] == 5) {
          this.hello.push('Started');
        } else if (this.status[i] == 6) {
          this.hello.push('Stopped');
        }
      }
      for (let i in this.status) {
        // image: any = [];
        if (this.status[i] == 0) {
          this.image.push('assets/img/icon/inactive.png');
        } else if (this.status[i] == 1) {
          this.image.push('assets/img/icon/active.png');
        } else if (this.status[i] == 2) {
          this.image.push('assets/img/icon/inprogress.png');
        } else if (this.status[i] == 3) {
          this.image.push('assets/img/icon/completed.png');
        } else if (this.status[i] == 4) {
          this.image.push('assets/img/icon/failed.png');
        } else if (this.status[i] == 5) {
          this.image.push('assets/img/icon/started.png');
        } else if (this.status[i] == 6) {
          this.image.push('assets/img/icon/stopped.png');
        }
      }
      // console.log(this.hello);
      // console.log(this.image);
    })).subscribe({next:(res:any)=>{
      console.log('next', res)
    }});
  }
  getServerList(){
    this.fd.getServerList(this.token).subscribe((res:any)=>{
      if(res.code===1){
        this.serverList = res.result;
        this.liveList.map(liv=>{
          liv.server_name = '';
          this.serverList.map(r=>{
            if(r.ip==liv.server){
              liv.server_name = r.name;
            }
          })
        })
        // this.liveForm.patchValue({
        //   server: this.serverList[0]['ip']
        // })
      }
    })
  }
  navigate(id,status){  
    this.router.navigate(['/add',id]);

      // if (status==0){
      //   this.router.navigate(['/add',id]);
      // }
      // else{
      //   Swal.fire({
      //     title: 'Not Allowed!',
      //     icon: 'warning',
      //     confirmButtonColor: '#3085d6',
      //     confirmButtonText: 'OK',
      //   })
      // }
    
  }
  
  postMainStatus(id:any, data) {
    // alert(data+id)
    console.log(data, 'asdfdf')
    Swal.fire({
      title: 'Are you sure?',
      text: 'You want to change the status!',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, change it!',
    }).then((result) => {
      if (result.isConfirmed) {
        const formData = new FormData();
        formData.append('job_type', 'live');
        formData.append('job_id', data.job_id);
        formData.append('field', (data.sendStatus=='server')?'server':(data.sendStatus=='ticker_text')?'ticker_text':'status');
        formData.append('value', (id=='0001')?this.ticker_text:id);
        // formData.append('server', data.server);

        this.fd.postStatus(this.token, formData).subscribe((res) => {
          console.log('upload', res);
          if (res.code == 1) {
            Swal.fire('Changed', 'Status Changed Successfully', 'success');
            this.TranscodingData();
            // const Toast = Swal.mixin({
            //   toast: true,
            //   position: 'top-end',
            //   showConfirmButton: false,
            //   timer: 3000,
            //   timerProgressBar: true,
            //   didOpen: (toast) => {
            //     toast.addEventListener('mouseenter', Swal.stopTimer)
            //     toast.addEventListener('mouseleave', Swal.resumeTimer)
            //   }
            // })

            // Toast.fire({
            //   icon: 'success',
            //   title: 'Status Changed Successfully'
            // })
            // Swal.fire({
            //   icon: 'success',
            //   title: 'Status Successfully Changed',
            // });
            // this.TranscodingData();
          }
        });
      }
    });
  }

  filterData(names) {
    // console.log(names.target.value)
    this.search = names.target.value;
    if (this.search == '') {
      this.TranscodingData();
    } else {
      this.fd.searchList(this.search, this.token).subscribe((res) => {
        this.liveList = res.result;
      });
    }
  }

  serverChange(item, list){
    list.server = item.value,
    list.sendStatus = 'server';

    this.postMainStatus(item.value, list)
  }
  
  selectedCheck(event, indx){
    console.log(event.target.id, indx);
    if(event.target.id==='listcheck_all'){
      let i = -1;
      this.liveList.forEach(element => {
        i++;
        this['listcheck_'+i+1] = document.getElementById('listcheck_'+(i+1));
        this['listcheck_'+i+1].checked = event.target.checked;
        element['is_selected']=this['listcheck_'+i+1].checked;
        // console.log(this['listcheck_'+i+1], i)
      });
    }else{
      this.liveList[indx].is_selected = event.target.checked;
      let changedList:any[] = this.liveList.filter(res=>res.is_selected);
      let listcheck_all:any = document.getElementById('listcheck_all');
      if(changedList.length == this.liveList.length){
        listcheck_all.checked = true;
      } else{
        listcheck_all.checked = false;
      }
      // this.liveList.forEach(element => {
        
      // });
    }
    this.job_ids = [];
    let changedList:any[] = this.liveList.filter(res=>res.is_selected);
    changedList.map(res=>res.is_selected?this.job_ids.push(res.job_id):this.job_ids=[]); 
    
  }
  addingSelection = `<option value="" selected disabled>Action</option>
  <option value="start">Start</option>
  <option value="stop">Stop</option>
  <option value="ticker">Ticker</option>
  <option value="delete">Delete</option>
  `;                                                            
  onChangeAction(selected_item){
    console.log(selected_item)
    let selectedAction: any = document.getElementById('selectedAction');
    let item = selected_item.target.value;
    this.job_ids = [];
    let changedList:any[] = this.liveList.filter(res=>res.is_selected);
    changedList.map(res=>res.is_selected?this.job_ids.push(res.job_id):this.job_ids=[]);  
    if(changedList.length>=1){
      if(changedList.length == this.liveList.length){
        let listcheck_all:any = document.getElementById('listcheck_all');
        listcheck_all.checked = true;
      }
      let data = {
        'sendStatus':'status',
        'job_id':this.job_ids.join(',')
      }
      switch (item) {
        case 'start':
          this.postMainStatus('5',data);
          break;    
        case 'stop':
          this.postMainStatus('6',data);
          break;
        case 'delete':
          this.postMainStatus('7',data);
          break;
        case 'ticker':
          Swal.fire({
            title: 'Ticker',
            html: `<input type="text" id="ticker_text" class="swal2-input" placeholder="Enter ticker text">`,
            confirmButtonText: 'Submit',
            focusConfirm: false,
            preConfirm: () => {
              const ticker_text:any = document.querySelector('#ticker_text');
              if (!ticker_text?.value) {
                Swal.showValidationMessage(`Please enter Ticker`)
              }
              return { ticker_text: ticker_text }
            }
          }).then((result:any) => {
            Swal.fire(`
              Ticker Text: ${result.value.ticker_text.value}
            `.trim());
              data['sendStatus']='ticker_text';
              this.ticker_text = result.value.ticker_text.value;
              this.postMainStatus('0001',data);
            });          
          break;
      }
    }else{
      selectedAction.innerHTML='';
      selectedAction.innerHTML = this.addingSelection;
      Swal.fire({
        title: 'Please Select Any Record',
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, change it!',
      });
    }
    console.log(this.job_ids,item.target.value, 'changedListdd');

  }
  playerSection = {
    player_url:'',
    thumb:'',
    channel_name:'',
  };

  playVideo(epgItem:any){
    this.playerSection = epgItem;
    this.playerSection.thumb = '';
    this.playStream(epgItem.player_url);
    // var player = new Clappr.Player({source: epgItem.url, parentId: "#player"});
  }
  playStream(stream) {
    var playerElement = document.getElementById("player-wrapper");
    let listPlayerBody:any = document.getElementById('listPlayerBody');
    let eW = listPlayerBody.clientWidth;
    let eH = Number(eW)*9/16;
    console.log(eW, eH)
    this.CPlayer = new Clappr.Player({
      source: stream,
      // source: 'http://recognizer.multitvsolution.com/streamer/dvr/mh1news-6/1644345000-3600.m3u8',
      poster: 'assets/poster.jpg',
      // mute: false,
      autoPlay: true,
      width: eW,
      height: 280,
      playInline: true,
      hideMediaControl: false,
      // hideSeekBar: true,
      // visibilityEnableIcon: false,

    });
    
    this.CPlayer.attachTo(playerElement);
    // $('#player-wrapper > div > .media-control').css({ 'height': '1'});
    
  }
  @HostListener('document:click', ['$event', '$event.target'])
    onClick(event: MouseEvent, targetElement: HTMLElement): void {
      let epgmodal:any=document.getElementById('listPlayerModal');
      if(targetElement===epgmodal){
        this.closeModal();
      }
  }
  closeModal(){
    let player:any = document.getElementById("player-wrapper");
    player.innerHTML='';
    this.is_thumb_opened = false;
  }
  copyUrl(item) {    
    console.log('pooku', item)
    if(item.thumb==''){
      navigator.clipboard.writeText(item.player_url);
    }else{
      navigator.clipboard.writeText(item.thumb);
    }
    const Toast = Swal.mixin({
      toast: true,
      position: 'top',
      showConfirmButton: false,
      timer: 1000,
      timerProgressBar: true,
      didOpen: (toast) => {
        toast.addEventListener('mouseenter', Swal.stopTimer)
        toast.addEventListener('mouseleave', Swal.resumeTimer)
      }
    })    
    Toast.fire({
      icon: 'success',
      title: 'Copied Successfully'
    })
  }
  openThumb(list){
    this.is_thumb_opened=true;
    this.playerSection = list;
  }
  openAlarm(list){
    this.selectedNotifyList = list;
    this.fd.getTranscodingNotify(this.token, list.job_id).subscribe((res:any)=>{
      if(res.code===1){
        this.notificationsList = res.result;        
        this.TranscodingData();
      }
    });    
  }
  closeNotifyModal(){
    const formData = new FormData();
    formData.append('job_id',this.selectedNotifyList.job_id);
    formData.append('status', 'read')
    this.fd.updateNotifyStatus(this.token, formData).subscribe((res:any)=>{
      if(res.code===1){
        this.TranscodingData();
        // Swal.fire('Done', 'Successfully Updated!', 'success');
      }
    })
  }
  gotoPages(page:any){
    if(typeof page === 'string'){
      let ind = this.pages.findIndex(res=>res.active==true);
      this.pages.map(res=>res.active=false);
      if(page === 'previous'){
        if(ind != 0){
          this.offset = this.offset -10;
          this.pages[ind-1]['active']=true;
        }else{
          this.offset = this.offset;
          this.pages[ind]['active']=true;
        }
      }else{
        if(ind != (this.pages.length-1)){
          this.offset = this.offset + 10;
          this.pages[ind+1]['active']=true;
        }else{
          this.offset = this.offset;
          this.pages[ind]['active']=true;
        }
      }
    }
    if(typeof page === 'object'){
      this.pages.map(res=>res == page ? res.active=true:res.active=false);
      let pindx = this.pages.findIndex(res=>res.active===true);
      this.offset = pindx * 10;
    }
    this.TranscodingData();
  }
  onChangeFilter(event:any){
    let id = event.target.value;
    let liveList = [];
    if(id==7){
      this.changeLiveList = this.liveList;
      liveList = this.liveList.filter(res=>res.notification_count>=1);
      this.liveList = liveList;
    }else{
      this.filterStatus = id;
      this.TranscodingData()
    }
    console.log(this.liveList,'pooku')
  }
}
