import { HttpClient } from '@angular/common/http';
import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import {
  FormBuilder,
  FormControl,
  FormGroup,
  NgForm,
  Validators,
} from '@angular/forms';
import { FetchService } from 'src/app/services/fetch.service';
import { map } from 'rxjs/operators';
import Swal from 'sweetalert2';
import { getLocaleDateFormat } from '@angular/common';
import { Params, Router } from '@angular/router';
import { event } from 'jquery';
import { ActivatedRoute } from '@angular/router';


@Component({
  selector: 'app-add',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.scss'],
})
export class AddComponent implements OnInit, AfterViewInit {
  disableTextbox=true;
  streamLineForm: FormGroup;

  font_familyLoad;
  font_family_name = 'select font';
  // selected_additional_options: any = [];
  additional_options:any = [];
  channelsList:any = [];
  public id: string;
  token = localStorage.getItem('token');
  datas = localStorage.getItem('data')
  liveForm: FormGroup;
  dropdownList: any = [];
  dropList: any = [];
  content: any = [];
  dropdownSettings
  dropdownSetting;
  isShown = false;
  isVisible: boolean;
  isShowed: boolean;
  isShow: false;
  Showed: boolean;
  newid
  select = [];
  isVis: false;
  authen: boolean;
  selectedType;
  cdnOption;
  authentication: boolean;
  submitForm:boolean;
  updateForm:boolean;
  dvrVisible = false;
  type;
  blackout_type;
  multichecked;
  dvrchecked;
  multilangualValue;
  logoFile;
  spriteFile;
  currentFileUpload;
  currentLoad;
  authh;
  cross = "X"
  name;
  isEnabled_spriteImage:any = false;
  logo_name;
  serverList:any[]=[];
  @ViewChild('formDir') private formDir: NgForm;
  selected_logos='choose Png';
  logos: any;

  constructor(
    private http: HttpClient,
    private fb: FormBuilder,
    private _FS: FetchService, private router: Router, private route: ActivatedRoute
  ) { }

  ngOnInit(): void {  
    this.streamLineForm = this.fb.group({
      channel_name: ['', Validators.required],
      input_type: ['', Validators.required],
      template: ['', Validators.required],
      output_type: ['', Validators.required],
      output_url: ['', Validators.required],
    });
    this.additional_options = ['Delete Segments','Round Durations'];

    // this.onLoadData();
    this.id = this.route.snapshot.paramMap.get('id')!;
    // alert(this.id)
    this.submitForm=true;
    this.updateForm=false;
    if(this.id != null){
      this.getInputdata();
      this.updateForm=true;
      this.submitForm=false;
    }

    this.getTemplates();
    this.liveForm = this.fb.group({
      id:[''],
      server:['', Validators.required],
      epg_channel:[''],
      catchup:[false],
      name: ['', Validators.required],
      type: ['', Validators.required],
      code: ['', Validators.required],
      quality: ['', Validators.required],
      interlacing: [''],
      rotate: [''],
      dvr: [false],
      window: ['7'],
      sprite: [''],
      sprite_image:[false],
      logo: [''],
      blackout: [''],
      template: ['', Validators.required],
      value: [''],
      width: [''],
      height: [''],
      top: [''],
      left: [''],
      ticker: [''],
      ticker_text:[''],
      ticker_width: [''],
      ticker_height: [''],
      bg_color: [''],
      text_color: [''],
      font_family: [''],
      font_size: [''],
      ticker_top: [''],
      ticker_left: [''],
      blackout_type: [''],
      blackout_value: [''],
      multichecked: [''],
      multilangualValue: [''],
      output: ['', Validators.required],
      url: ['', Validators.required],
      num_retries: [''],
      retry_interval: [''],
      auth: [''],
      auth2: [''],
      username: [''],
      password: [''],
      cdn_type: [''],
      modifier: [''],
      directory: [''],
      segment_length: [''],
      segment_list: [''],
      hls_url: [''],
      options: [''],
      aut: [''],
      ticker_bool:[false],
      is_authenticated:[false]
    });
    this.multichecked = 0;
    this.dvrchecked = 0;
    this.authh = 0;
    this.isVisible =false;
    this.isShowed=false;
    this.Showed=false;
    this.authen=false;
    this.authentication=false;

    this.dropdownSettings = {
      singleSelection: false,
      idField: 'item_id',
      textField: 'item_text',
      allowSearchFilter: true,
    };
    this.dropdownSetting = {
      singleSelection: false,
      idField: 'id',
      textField: 'text',
      allowSearchFilter: true,
    };

    this.dropList = [
      { id: 1, text: 'Delete Segments' },
      { id: 2, text: 'Round Duration' },
    ]

    this.liveForm.get('logo')?.valueChanges.subscribe(res=>{
      this.isEnabled_spriteImage = res;
    });
    
  }
  streamFormSubmit(){
    console.log(this.streamLineForm.value)
  }
  
  onLoadData(){
    this.route.params.subscribe((params:Params)=>{
      let id = params['id'];
      if(Number(id)>0){
        this._FS.getData(this.token, id).subscribe((res: any) => {
          const streamDataObj = res.result[0];
          for (const key in streamDataObj) {
            this.streamLineForm = this.fb.group({
              [key]: new FormControl(streamDataObj[key])
            })
            // this.streamLineForm.addControl(`${key}`, new FormControl(`${streamDataObj[key]}`, Validators.required));
            // console.log(this.streamLineForm.controls, 'just imagine');
          }

          // this.streamLineForm.patchValue({
          //   channel_name: res.result[0]['channel_name'],
          //   input_type: res.result[0]['input_type'],
          //   template: res.result[0]['template'],
          //   output_type: res.result[0]['output_type'],
          //   output_url: res.result[0]['output_url'],        
          // })
        });
      }
    })
    
  }
  ngAfterViewInit(): void {
    this._FS.getChannels(this.token).subscribe((res:any)=>{
      if(res.code===1){
        this.channelsList = res.result;
      }
    });
    this.getServerList();
  }
  getServerList(){
    this._FS.getServerList(this.token).subscribe((res:any)=>{
      if(res.code===1){
        this.serverList = res.result;
        this.liveForm.patchValue({
          server: this.serverList[0]['ip']
        })
      }
    })
  }
  toggleShow(e) {
    this.isShown = e.target.checked;
  }
  toggleShowed(e) {
    this.isShowed = e.target.checked;
  }
  toggle(e) {
    this.isShow = e.target.checked;
    this.Showed = false;
  }
  togglee(e) {
    this.Showed = e.target.checked;
    this.isShow = false;
  }
  multiShow(e) {
    this.isVis = e.target.checked;
  }
  dvrShow(e) {
    this.dvrVisible = e.target.checked;
  }
  showOptions(e) {
    this.authen = e.target.checked;
  }
  showwOptions(e) {
    this.authentication = e.target.checked;
  }
  output(event) {
    this.selectedType = event.target.value;

  }
  outputs(event) {
    this.cdnOption = event.target.value;
  }

  func(data) {
    this.logoFile = data.target.files;
    let logoSelection:any[] = [];
    for(let i of data.target.files){
      logoSelection.push(i.name);
    }
    this.selected_logos = logoSelection.join(',');

    // console.log(this.selected_logos, data.target.files[0].name);

    this.currentFileUpload = this.logoFile.item(0);
    if (data.isTrusted == true) {
      this.isVisible = !this.isVisible
    }
  }
  function(data) {
    this.spriteFile = data.target.files;
    this.currentLoad = this.spriteFile.item(0);
  }
  fontSelection(data){
    console.log(data)
    let file = data.target.files;
    this.font_familyLoad = file.item(0);
  }

  getTemplates() {
    this._FS.getTemplateData(this.token).pipe(map((data) => {         
      for (let inp of data.result) {
        this.dropdownList = this.dropdownList.concat({
          item_id: inp.template_id,
          item_text: inp.title,
        });
      }
    })).subscribe();
  }

  Getdata(event) {
    if (event.isTrusted == true) {
      this.authh = 1;
    } else {
      this.authh = 0;
    }
  }


  GetStats(event) {
    this.type = event.target.value;
    console.log(this.type);
    if (this.type == 'SDI') {
      this.isShown = false;
    }
  }

  multiChecked(event) {
    if (event.isTrusted == true) {
      this.multichecked = 1;
    }
    else {
      this.multichecked = 0;
    }
  }
  dvrChecked(event) {
    if (event.isTrusted == true) {
      this.dvrchecked = 1;
    }
    else {
      this.dvrchecked = 0;
    }
  }

  getBlackout(event) {
    this.blackout_type = event.target.value;
    console.log(this.blackout_type);
  }
  selectedItems: any = [];
  selected: any = [];


  GetVal(event) {
    this.multilangualValue = event.target.value;
  }

  getInputdata() {
    if (this.id != '') {
      this._FS.getData(this.token, this.id).subscribe((res: any) => {
        if(res.code === 1){

        }
        this.content = res.result[0];
        this.logo_name = res.result[0]['logo_name'];
        this.selected_logos = (this.logo_name!='')?this.logo_name:this.selected_logos;
        this.font_family_name = res.result[0]['ticker_font_family_name'];
        this.additional_options = ['Delete Segments','Round Durations'];
        this.selectedItems = res.result[0]['additional_options'];
        
        // console.log(this.content); 
        this.liveForm.controls['id'].setValue(res.result[0].id);       
        this.newid=res.result[0].job_id || this.id;
        this.liveForm.controls['name'].setValue(res.result[0].channel_name);
        this.liveForm.controls['server'].setValue(res.result[0].server);
        this.liveForm.controls['bg_color'].setValue(res.result[0].ticker_bg_color);
        this.liveForm.controls['text_color'].setValue(res.result[0].ticker_text_color);
        this.liveForm.controls['font_size'].setValue(res.result[0].ticker_font_size);
        this.liveForm.controls['output'].setValue(res.result[0].output_type);
        this.liveForm.controls['url'].setValue(res.result[0].output_url);
        this.liveForm.controls['type'].setValue(res.result[0].input_type);
        this.liveForm.controls['code'].setValue(res.result[0].time_code);
        this.liveForm.controls['quality'].setValue(res.result[0].quality);
        this.liveForm.controls['interlacing'].setValue(res.result[0].interlacing);
        this.liveForm.controls['rotate'].setValue(res.result[0].rotate);
        this.liveForm.controls['value'].setValue(res.result[0].input_value);
        this.liveForm.controls['blackout_type'].setValue(res.result[0].blackout_type);
        this.liveForm.controls['username'].setValue(res.result[0].authentication_username);
        this.liveForm.controls['password'].setValue(res.result[0].authentication_password);
        this.liveForm.controls['cdn_type'].setValue(res.result[0].cdn_type);
        this.liveForm.controls['modifier'].setValue(res.result[0].name_modifier);
        this.liveForm.controls['segment_list'].setValue(res.result[0].segment_list);
        this.liveForm.controls['segment_length'].setValue(res.result[0].segment_length);
        this.liveForm.controls['directory'].setValue(res.result[0].directory_structure);
        this.cdnOption = res.result[0].directory_structure;
        // console.log(this.cdnOption, 'ppppp')
        this.liveForm.controls['hls_url'].setValue(res.result[0].hls_baseurl);
        this.liveForm.controls['sprite_image'].setValue((res.result[0].sprite_image==1)?true:false);
        this.liveForm.controls['catchup'].setValue((res.result[0].catchup==1)?true:false);
        this.liveForm.controls['epg_channel'].setValue(res.result[0].epg_channel);
        this.liveForm.controls['ticker_bool'].setValue((res.result[0].ticker==1)?true:false);
        this.liveForm.controls['dvr'].setValue((res.result[0].dvr==1)?true:false);
        this.liveForm.controls['ticker_text'].setValue((res.result[0].ticker_text));
        this.liveForm.controls['is_authenticated'].setValue((res.result[0].authentication==1)?true:false);
        // this.liveForm.controls['logo'].setValue(res.result[0].logo);
        // console.log(this.liveForm.value)
        if(res.result[0].input_type=='Network Input'){
          this.isShown=true
          this.type='Network Input'
        }
        if(res.result[0].input_type=='SDI'){
          this.isShown=false
          this.type='SDI'
        }
        if(res.result[0].dvr==1){
          this.liveForm.controls['dvr'].setValue(res.result[0].dvr);
          this.dvrchecked = 1;
          this.dvrVisible=true
          this.liveForm.controls['window'].setValue(res.result[0].dvr_window);
        }
        if(res.result[0].logo!=''){
          this.logos = res.result[0].logo;     
          
          this.isVisible=true
          var resolution = res.result[0].logo_resolution.split('X');
          // console.log(resolution, 'pooku')
          this.liveForm.controls['width'].setValue(((resolution[0]=='undefined')||(resolution[0]==undefined))?'':resolution[0]);
          this.liveForm.controls['height'].setValue(((resolution[1]=='undefined')||(resolution[1]==undefined))?'':resolution[1]);
          var offset =res.result[0].logo_offset.split('X')
          this.liveForm.controls['top'].setValue(offset[0]);
          this.liveForm.controls['left'].setValue(offset[1]);
        }
        if(res.result[0].blackout_type=='Text'){
          this.blackout_type='Text'
          this.Showed =true
          this.liveForm.controls['blackout_value'].setValue(res.result[0].blackout_value);
        }
        if(res.result[0].output_type=='RTMP'){
          this.selectedType ='RTMP'
          this.liveForm.controls['num_retries'].setValue(res.result[0].num_retries);
          this.liveForm.controls['retry_interval'].setValue(res.result[0].retry_interval);
          if(res.result[0].authentication=='1'){
            this.authen=true
            this.liveForm.controls['aut'].setValue(res.result[0].authentication);
            }
        }
        else if(res.result[0].output_type=='HLS'){
          this.selectedType ='HLS'
          if(res.result[0].authentication=='1'){
            this.authentication=true
            this.liveForm.controls['aut'].setValue(res.result[0].authentication);
            }

        }
      
      

        for (let inp of res.result[0].template) {
          
          this.selected = this.selected.concat({
            item_id: inp.template_id,
            item_text: inp.title,
          });
        }



      })
    }
  }
  removeLogo(){
    this.logo_name = '';
    this.selected_logos='choose Png';
  }
  onSubmit() {
    console.log(this.liveForm.valid)
    const formData = new FormData();
    let selectvar: any = [];
    this.selected.forEach((element: any) => {
      selectvar.push(element.item_id);
    });

    let selecvar: any = [];
    this.selectedItems.forEach((element: any) => {
      selecvar.push(element.text);
    });
    for (const key in this.liveForm.controls) {
      if(this.liveForm.value[key]==''){
        // console.log('pooku')
        this.liveForm.value[key]='';
        // console.log();
      }
    }
    for(let key in this.liveForm.controls){
      
      // if((key=='channel_name')||(key=='type')||(key=='template')||(key=='output')||(key=='url')){
      //   this.liveForm.get(key)?.addValidators();               

      // }else{
      //   this.liveForm.get(key)?.clearValidators();               
      // }
    }
    if (this.liveForm.valid) {      
      formData.append('channel_name', this.liveForm.value.name);
      formData.append('input_type', this.liveForm.value.type);
      formData.append('input_value', this.liveForm.value.value);
      if (this.liveForm.value.width == "") {
        this.cross = ""
      }
      else {
        this.cross = "X"
      }
      formData.append('logo_resolution', this.liveForm.value.width + this.cross + this.liveForm.value.height);
      if (this.liveForm.value.top == "") {
        this.cross = ""
      }
      else {
        this.cross = "X"
      }
      formData.append('logo_offset', this.liveForm.value.top + this.cross + this.liveForm.value.left);
      formData.append('ticker', (this.liveForm.value.ticker_bool)?'1':'0');
      formData.append('ticker_text', this.liveForm.value.ticker_text);
      if (this.liveForm.value.ticker_width == "") {
        this.cross = ""
      }
      else {
        this.cross = "X"
      }
      formData.append('ticker_resolution', this.liveForm.value.ticker_width + this.cross + this.liveForm.value.ticker_height);
      formData.append('ticker_bg_color', this.liveForm.value.bg_color);
      formData.append('ticker_text_color', this.liveForm.value.text_color);
      // formData.append('ticker_font_family', this.liveForm.value.font_family); /*need to make it file and optional */
      // formData.append('ticker_font_family', this.font_familyLoad); /*need to make it file and optional */
      formData.append('ticker_font_family', ((this.font_familyLoad=='')||(this.font_familyLoad==undefined))?'':this.font_familyLoad); /*need to make it file and optional */
      formData.append('ticker_font_size', this.liveForm.value.font_size); /*need to make it dropdown range 30 - 40 */
      if (this.liveForm.value.ticker_top == "") {
        this.cross = ""
      }
      else {
        this.cross = "X"
      }
    
      formData.append('ticker_offset', this.liveForm.value.ticker_top + this.cross + this.liveForm.value.ticker_left);
      formData.append('template', selectvar);
      // formData.append('blackout_type', this.blackout_type);
      formData.append('blackout_value', this.liveForm.value.blackout_value);
      formData.append('multilangual', this.multichecked);
      // formData.append('multilangual_value', this.multilangualValue);
      formData.append('multilangual_value', ((this.multilangualValue=='')||(this.multilangualValue==undefined))?'':this.multilangualValue);

      formData.append('output_type', this.liveForm.value.output);
      formData.append('output_url', this.liveForm.value.url);
      formData.append('authentication', (this.liveForm.value.is_authenticated)?'1':'0');
      formData.append('num_retries', this.liveForm.value.num_retries);
      formData.append('retry_interval', this.liveForm.value.retry_interval);
      formData.append('authentication_password', this.liveForm.value.password);
      formData.append('authentication_username', this.liveForm.value.username);
      formData.append('cdn_type', this.liveForm.value.cdn_type);
      formData.append('name_modifier', this.liveForm.value.modifier);
      formData.append('directory_structure', this.liveForm.value.directory);
      formData.append('segment_length', this.liveForm.value.segment_length);
      formData.append('segment_list', this.liveForm.value.segment_list);
      formData.append('hls_baseurl', this.liveForm.value.hls_url);
      formData.append('additional_options', this.selectedItems);
      // formData.append('logo', this.currentFileUpload);
      formData.append('logo', ((this.currentFileUpload==undefined)||(this.currentFileUpload==''))?'':this.currentFileUpload);     

      formData.append('time_code', this.liveForm.value.code);
      formData.append('quality', this.liveForm.value.quality);
      formData.append('interlacing', this.liveForm.value.interlacing);
      formData.append('rotate', this.liveForm.value.rotate);
      // formData.append('dvr', this.dvrchecked);
      formData.append('dvr', (this.liveForm.value.dvr)?'1':'0');
      formData.append('server', this.liveForm.value.server);
      formData.append('dvr_window', this.liveForm.value.window);
      formData.append('catchup', this.liveForm.value.catchup?'1':'0');
      formData.append('epg_channel', this.liveForm.value.epg_channel);
      // formData.append('sprite_image', this.currentLoad);
      formData.append('sprite_image', (this.liveForm.value.sprite_image == true)?'1':'0');
      this._FS.submitTranscoding(this.token, formData).subscribe((res: any) => {
        this.liveForm.reset();
        this.formDir.resetForm();
        Swal.fire({
          icon: 'success',
          title: 'Successfully Submitted',
        });
        this.router.navigate(['list']);
      });
    }
  }
  selections(e){
    console.log(e, 'pooka')
  }
  onUpdate(){
    // for(let i in this.liveForm.controls){
    //   // console.log(this.liveForm.controls[i].value, 'kjhg');
    //   // if((this.liveForm.controls[i].value=='')||(this.liveForm.controls[i].value==undefined)){
    //   //   this.liveForm.controls[i].patchValue({
    //   //     [i]:null
    //   //   });
    //   //   console.log(this.liveForm.controls[i].value, 'gggg');
    //   // }
    // }
    const formData = new FormData();
    let selectvar: any = [];
    this.selected.forEach((element: any) => {
      selectvar.push(element.item_id);
    });

    let selecvar: any = [];
    this.selectedItems.forEach((element: any) => {
      selecvar.push(element.text);
    });
    
    if (this.liveForm.valid) {
     
      formData.append('job_id',this.newid);      
      formData.append('channel_name', this.liveForm.value.name);
      formData.append('input_type', this.type);
      formData.append('input_value', this.liveForm.value.value);
      if (this.liveForm.value.width == "") {
        this.cross = ""
      }
      else {
        this.cross = "X"
      }
      formData.append('logo_resolution', this.liveForm.value.width + this.cross + this.liveForm.value.height);
      if (this.liveForm.value.top == "") {
        this.cross = ""
      }
      else {
        this.cross = "X"
      }
      formData.append('logo_offset', this.liveForm.value.top + this.cross + this.liveForm.value.left);
      // formData.append('ticker', this.liveForm.value.ticker);
      formData.append('ticker', (this.liveForm.value.ticker_bool)?'1':'0');
      formData.append('ticker_text', this.liveForm.value.ticker_text);

      if (this.liveForm.value.ticker_width == "") {
        this.cross = ""
      }
      else {
        this.cross = "X"
      }
      formData.append('ticker_resolution', this.liveForm.value.ticker_width + this.cross + this.liveForm.value.ticker_height);
      formData.append('ticker_bg_color', this.liveForm.value.bg_color);
      formData.append('ticker_text_color', this.liveForm.value.text_color);
      // formData.append('ticker_font_family', this.liveForm.value.font_family);
      formData.append('ticker_font_family', ((this.font_familyLoad=='')||(this.font_familyLoad==undefined))?'':this.font_familyLoad); /*need to make it file and optional */

      formData.append('ticker_font_size', this.liveForm.value.font_size);
      if (this.liveForm.value.ticker_top == "") {
        this.cross = ""
      }
      else {
        this.cross = "X"
      }
      formData.append('ticker_offset', this.liveForm.value.ticker_top + this.cross + this.liveForm.value.ticker_left);
      formData.append('template', selectvar);
      // formData.append('blackout_type', this.blackout_type);
      // formData.append('blackout_value', this.liveForm.value.blackout_value);
      formData.append('multilangual', this.multichecked);
      formData.append('multilangual_value', ((this.multilangualValue=='')||(this.multilangualValue==undefined))?'':this.multilangualValue);
      formData.append('output_type', this.liveForm.value.output);
      formData.append('output_url', this.liveForm.value.url);
      // formData.append('authentication', this.authh);
      formData.append('authentication', (this.liveForm.value.is_authenticated)?'1':'0');
      formData.append('catchup', (this.liveForm.value.catchup)?'1':'0');
      formData.append('epg_channel', this.liveForm.value.epg_channel);
      formData.append('server', this.liveForm.value.server);
      formData.append('id', this.liveForm.value.id);

      formData.append('num_retries', this.liveForm.value.num_retries);
      formData.append('retry_interval', this.liveForm.value.retry_interval);
      formData.append('authentication_password', this.liveForm.value.password);
      formData.append('authentication_username', this.liveForm.value.username);
      formData.append('cdn_type', this.liveForm.value.cdn_type);
      formData.append('name_modifier', this.liveForm.value.modifier);
      formData.append('directory_structure', this.liveForm.value.directory);
      formData.append('segment_length', this.liveForm.value.segment_length);
      formData.append('segment_list', this.liveForm.value.segment_list);
      formData.append('hls_baseurl', this.liveForm.value.hls_url);
      formData.append('additional_options', this.selectedItems);     
      formData.append('logo', ((this.currentFileUpload==undefined)||(this.currentFileUpload==''))?'':this.currentFileUpload);     
      formData.append('logo_url', this.logo_name);
      formData.append('time_code', this.liveForm.value.code);
      formData.append('quality', this.liveForm.value.quality);
      formData.append('interlacing', this.liveForm.value.interlacing);
      formData.append('rotate', this.liveForm.value.rotate);
      formData.append('dvr', (this.liveForm.value.dvr)?'1':'0');
      formData.append('dvr_window', this.liveForm.value.window);
      formData.append('ticker_font_family_url', this.font_family_name);
      
      // formData.append('sprite_image', this.currentLoad);
      formData.append('sprite_image', (this.liveForm.value.sprite_image == true)?'1':'0');
    // console.log(this.logo_name,)
      this._FS.updateTranscoding(this.token, formData).subscribe((res: any) => {
        this.liveForm.reset();
        this.formDir.resetForm();
        Swal.fire({
          icon: 'success',
          title: 'Successfully Updated',
          timer: 3000
        });
        this.router.navigate(['list']);
      });
    }
  }
  
}
