import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { EpgComponent } from './epg.component';

const routes: Routes = [
  {path:'', component:EpgComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EpgRoutingModule { }
