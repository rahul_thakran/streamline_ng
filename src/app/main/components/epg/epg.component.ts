import { Component, HostListener, OnInit } from '@angular/core';
import { FetchService } from 'src/app/services/fetch.service';
import { EpgTime } from './epg.time';
declare var $: any;
import * as Clappr from 'clappr';

@Component({
  selector: 'app-epg',
  templateUrl: './epg.component.html',
  styleUrls: ['./epg.component.scss']
})
export class EpgComponent implements OnInit {
  CPlayer:any;
  epgTime: string[] = EpgTime;
  ddToday = new Date().toLocaleString("en-GB").substring(0, 2);
  mmToday = new Date().toLocaleString("en-GB").substring(3, 5);
  yyyyToday = new Date().toLocaleString("en-GB").substring(6, 10);
  today = this.yyyyToday + '-' + this.mmToday + '-' + this.ddToday; 
  epgChannels:any[] = [];
  moreInfo = false;
  details;
  time = new Date().toLocaleTimeString("en-GB");
  hour: any = this.time.substring(0,2);
  min: any = this.time.substring(3, 5);
  liveChange = false;
  epgChannelStatus = false;
  epoch = new Date().getTime();  

  constructor(private _FS:FetchService) { }

  ngOnInit(): void {
    this.getEpgChannels();
    this.dragScroll();
  }

  getEpgChannels(){
    this.epgChannelStatus = true;
    this._FS.getEpgChannels(this.today).subscribe((res:any)=>{
      if(res.code===1){
        this.liveChange = true;
        this.epgChannelStatus = false;
        // this.epgChannels = res.result['channels'];
        this.epgChannels = res.result;
        let items:any = document.querySelector('.items');
        items.scrollLeft = new Date().getHours() * 60 * 8;
      }
      // console.log(res.result['channels'], 'pooku')
      // this.epgChannels.forEach(element => {
      //   element['epgs'].forEach(list => {
      //     list['epg'].forEach(epg => {
      //       let epoch = new Date(`${epg['progdate']} ${epg['progtime']}`).getTime()/1000;
      //       epg['epoc']=epoch;
      //       // console.log(new Date(`${epg['progdate']} ${epg['progtime']}`).getTime())
      //     });
      //   });
        
      // });
    })
  }
  getMoreInfo(id, id2) {
    setTimeout(() => {
      this.moreInfo = true;
      this.details = this.epgChannels[id].epg[id2].synopsis;
    }, 1000);
  }
  dragScroll() {
    const slider: any = document.querySelector('.items');
    let isDown = false;
    let startX;
    let scrollLeft;
    
    slider.addEventListener('mousedown', (e: any) => {
      isDown = true;
      slider.classList.add('active');
      startX = e.pageX - slider.offsetLeft;
      scrollLeft = slider.scrollLeft;
    });
    slider.addEventListener('mouseleave', () => {
      isDown = false;
      slider.classList.remove('active');
    });
    slider.addEventListener('mouseup', () => {
      isDown = false;
      slider.classList.remove('active');
    });
    slider.addEventListener('mousemove', (e) => {
      if(!isDown) return;
      e.preventDefault();
      const x = e.pageX - slider.offsetLeft;
      const walk = (x - startX) * 3; //scroll-fast
      slider.scrollLeft = scrollLeft - walk;
    });

    const rightBtn:any = document.querySelector('#right-button');
    const leftBtn:any = document.querySelector('#left-button');
    const content:any = document.querySelector('.items');

    let scrollAmount = slider.scrollLeft;
    let scrollMin = 0
    let scrollMax = content.clientWidth;

    rightBtn.addEventListener("click", function(event) {
      content.scrollLeft += (content.clientWidth - 120);
      // content.scrollTo({
      //   top: 0,
      //   left: Math.min(scrollAmount += 600),
      //   behavior: 'smooth'
      // });
      
      event.preventDefault();
    });
    
    leftBtn.addEventListener("click", function(event) {
      content.scrollLeft -= (content.clientWidth - 120);      
      event.preventDefault();
      // content.scrollTo({
      //   top: 0,
      //   left: Math.min(scrollAmount -= 600),
      //   behavior: 'smooth'
      // });
    });    
  }
  epgSection = {
    title:''
  };

  playVideo(epgItem:any){
    this.epgSection = epgItem;
    console.log(epgItem)
    this.playStream(epgItem.url);
    // var player = new Clappr.Player({source: epgItem.url, parentId: "#player"});
  }
  playStream(stream) {
    var playerElement = document.getElementById("player-wrapper");
    let epgBody:any = document.getElementById('epgBody');
    let eW = epgBody.clientWidth;
    let eH = Number(eW)*9/16;
    console.log(eW, eH)
    this.CPlayer = new Clappr.Player({
      source: stream,
      // source: 'http://recognizer.multitvsolution.com/streamer/dvr/mh1news-6/1644345000-3600.m3u8',
      poster: 'assets/poster.jpg',
      // mute: false,
      autoPlay: true,
      width: eW,
      height: 280,
      playInline: true,
      hideMediaControl: false,
      // hideSeekBar: true,
      // visibilityEnableIcon: false,

    });
    
    this.CPlayer.attachTo(playerElement);
    // $('#player-wrapper > div > .media-control').css({ 'height': '1'});
    
  }
  @HostListener('document:click', ['$event', '$event.target'])
    onClick(event: MouseEvent, targetElement: HTMLElement): void {
      let epgmodal:any=document.getElementById('epgPlayModal');
      if(targetElement===epgmodal){
        this.closeModal();
      }
  }
  closeModal(){
    let player:any = document.getElementById("player-wrapper");
    player.innerHTML='';
  }
  downloadEpg(epg) {
    const formData = new FormData();
    const channel = epg.channel.replace(/\s/g, '');
    const user_id = JSON.parse(localStorage.getItem('streamLineUser') || '{}')['id'];
    const token = JSON.parse(localStorage.getItem('streamLineUser') || '{}')['token'];
    // formData.append('program_title', epg.title);    
    formData.append('program_description', epg.synopsis);    
    formData.append('program_thumb', epg['programme-thumb']);    
    formData.append('channelname', channel);    
    formData.append('starttime', epg.progdate + ' ' + epg.progtime);    
    formData.append('duration', epg.duration);    
    formData.append('url', epg.url);
    formData.append('user_id', user_id);
    formData.append('token', token);
    if (epg.url) {
      this._FS.downloadEpg(formData).subscribe(res => {
        console.log('epg', res);
      })
    }  
  } 
}
