import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { EpgRoutingModule } from './epg-routing.module';
import { EpgComponent } from './epg.component';
import { RouterModule } from '@angular/router';


@NgModule({
  declarations: [
    EpgComponent
  ],
  imports: [
    CommonModule,
    RouterModule,
    EpgRoutingModule
  ]
})
export class EpgModule { }
