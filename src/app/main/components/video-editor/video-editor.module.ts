import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { VideoEditorRoutingModule } from './video-editor-routing.module';
import { VideoEditorComponent } from './video-editor.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {DateService, DateTimePickerModule} from 'ngx-datetime-picker';
import {DpDatePickerModule} from 'ng2-date-picker';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { TimepickerModule } from 'ngx-bootstrap/timepicker';
import { PopoverModule } from 'ngx-bootstrap/popover';
import { NgxSliderModule } from "@angular-slider/ngx-slider";

@NgModule({
  declarations: [VideoEditorComponent],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    DateTimePickerModule,
    VideoEditorRoutingModule,
    DpDatePickerModule,
    BsDatepickerModule.forRoot(),
    TimepickerModule.forRoot(),
    PopoverModule.forRoot(),
    NgxSliderModule
  ],
  providers: [
    DateService
  ],
})
export class VideoEditorModule { }
