import { ChangeContext, LabelType, Options } from '@angular-slider/ngx-slider';
import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
declare var $: any;
import * as Clappr from 'clappr';
import { DateService } from 'ngx-datetime-picker';
import { FetchService } from 'src/app/services/fetch.service';

@Component({
  selector: 'app-video-editor',
  templateUrl: './video-editor.component.html',
  styleUrls: ['./video-editor.component.scss']
})
export class VideoEditorComponent implements OnInit {
  CPlayer:any;
  channelsList = [];
  durationTime = [
    { 'name': 1 },
    { 'name': 5 },
    { 'name': 10 },
    { 'name': 30 },
    { 'name': 60 },
    { 'name': 90 },
    { 'name': 120 },
    { 'name': 150 },
    { 'name': 180 },
  ];
  selectedTime = 60;
  selectedChannel;
  myDate;
  dateNow = new Date();
  epoch = new Date().getTime();  
  // yesterdayEpoch = new Date(this.epoch - 24 * 60 * 60 * 1000);
  todayEpoch = new Date(this.epoch - 2 * 60 * 60 * 1000);
  dynamicDate = this.todayEpoch;
  videoStatus = false;
  minimumDate = new Date().getTime() - (90 * 24 * 60 * 60 * 1000);
  maximumDate = new Date().getTime();
  minDate = new Date(this.minimumDate);
  maxDate = new Date(this.maximumDate);
  previewPlay = false;
  previewEpoch;

  editorForm = new FormGroup({
    epg_id: new FormControl(''),
    channel: new FormControl(''),
    catchup_name: new FormControl(''),
    date: new FormControl(''),
    duration: new FormControl('')
  });
  start: number = 0;
  end: number = 100;
  showButtons = false;
  options: Options = {
    floor: 0,
    ceil: this.end,
    step: 0.1,
    animate: false,
    draggableRange: true
  };

  videoCutterForm = new FormGroup({
    program_title: new FormControl(''),
    program_description: new FormControl(''),
    program_thumb: new FormControl(''),
    token: new FormControl(''),
    user_id: new FormControl(''),
    start_time: new FormControl(''),
    m3u8Url: new FormControl(''),
    url: new FormControl(''),
    title: new FormControl('', [ Validators.required]),
    channel_name: new FormControl(''),
    start: new FormControl(''),
    end: new FormControl('', Validators.required)
  });
  playing;
  seekPlay = false;
  logText;
  startTime: number;
  endTime: number;
  formStatus = false;
  epgToken;
  timer:Date;

  constructor(private dateService: FetchService) { }

  
  
  ngOnInit(): void {
    let stream = 'https://www.w3schools.com/html/mov_bbb.mp4';
    this.playStream(stream);
    
  }
  changeDate(e){

  }
  editorSubmit(){
    console.log(this.editorForm.value)
  }

  playStream(stream) {
    var playerElement:any = document.getElementById("player-wrapper");
    let editorBody:any = document.getElementById('editorBody');
    playerElement.innerHTML = '';
    let eW = editorBody.clientWidth;
    let eH = Number(eW)*9/16 -10;
    this.CPlayer = new Clappr.Player({
      source: stream,
      // source: 'http://recognizer.multitvsolution.com/streamer/dvr/mh1news-6/1644345000-3600.m3u8',
      poster: 'assets/poster.jpg',
      // mute: false,
      autoPlay: true,
      width: eW,
      height: eH,
      playInline: true,
      hideMediaControl: false,
      events: {
        onReady: ()=> {
          setTimeout(() => {
            this.end = this.CPlayer.getDuration();
            this.options = {
              floor: 0,
              ceil: this.end,
              step: 0.1,
              minRange: .1,
              draggableRange: true,
              animate: false,
              showSelectionBar: true,
              // translate: (value: number, label: LabelType): string => {
              //   switch (label) {
              //     case LabelType.Low:
              //       return 'Min time: ' + new Date(this.previewEpoch + (value * 60 * 1000)).toLocaleTimeString();
              //     case LabelType.High:
              //       return 'Max time: ' + new Date(this.previewEpoch + (value * 60 * 1000)).toLocaleTimeString();
              //     default:
              //       return value + ' Min';
              //   }
              // }
            }
            console.log(this.CPlayer.getDuration(), this.end, 'pooku');

          }, 100);
        }
      }
      // hideSeekBar: true,
      // visibilityEnableIcon: false,

    });
    
    this.CPlayer.attachTo(playerElement);
        
    // $('#player-wrapper > div > .media-control').css({ 'height': '1'});
    
  }
  onUserChange(changeContext: ChangeContext): void {
    let start = changeContext.value;
    let end = changeContext.highValue;
    if (changeContext.pointerType === 0) {     
      this.CPlayer.seek(start);
      this.startTime = changeContext.value;
      
    } 
    if(changeContext.pointerType === 1) {
      this.CPlayer.seek(end);
      this.endTime = Number(end);
    }
         console.log(changeContext.highValue, changeContext.pointerType);
  }
}
