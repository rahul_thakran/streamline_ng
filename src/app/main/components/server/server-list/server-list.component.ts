import { Component, OnInit } from '@angular/core';
import { FetchService } from 'src/app/services/fetch.service';
import Swal from 'sweetalert2';
@Component({
  selector: 'app-server-list',
  templateUrl: './server-list.component.html',
  styleUrls: ['./server-list.component.scss']
})
export class ServerListComponent implements OnInit {
  token = localStorage.getItem('token');
  serverList:any[] =[];
  constructor(private _FS:FetchService) { }

  ngOnInit(): void {
    this.getServerList();
  }

  getServerList(){
    this._FS.getServerList(this.token).subscribe((res:any)=>{
      if(res.code===1){
        this.serverList = res.result;
      }
    })
  }
  deleteRecord(record){
    let index = this.serverList.findIndex(res=>res==record);
    const formData = new FormData();
    formData.append('id', record.id);
    formData.append('field', 'status');
    formData.append('value', '2');
    
    Swal.fire({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!'
    }).then((result) => {
      if (result.isConfirmed) {
        this._FS.deleteServer(this.token, formData).subscribe(res=>{
          if(res.code===1){
            this.serverList.splice(index, 1); 
            Swal.fire(
              'Deleted!',
              'Your file has been deleted.',
              'success'
            );
          }
        });         
        
      }
    })
  }
}
