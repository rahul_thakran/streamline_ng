import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ServerRoutingModule } from './server-routing.module';
import { ServerComponent } from './server.component';
import { ServerAddComponent } from './server-add/server-add.component';
import { ServerListComponent } from './server-list/server-list.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';


@NgModule({
  declarations: [
    ServerComponent,
    ServerAddComponent,
    ServerListComponent
  ],
  imports: [
    CommonModule,
    ServerRoutingModule,
    FormsModule,
    ReactiveFormsModule
  ]
})
export class ServerModule { }
