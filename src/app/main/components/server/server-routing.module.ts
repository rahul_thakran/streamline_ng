import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ServerAddComponent } from './server-add/server-add.component';
import { ServerListComponent } from './server-list/server-list.component';
import { ServerComponent } from './server.component';

const routes: Routes = [
  { path:'',
    component:ServerComponent,
    children:[
      {path:'add', component:ServerAddComponent},
      {path:'list', component:ServerListComponent},
    ]}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ServerRoutingModule { }
