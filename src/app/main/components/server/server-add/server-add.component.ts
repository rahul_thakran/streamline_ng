import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, NgForm, Validators } from '@angular/forms';
import { FetchService } from 'src/app/services/fetch.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-server-add',
  templateUrl: './server-add.component.html',
  styleUrls: ['./server-add.component.scss']
})
export class ServerAddComponent implements OnInit {
  serverForm:FormGroup;
  token = localStorage.getItem('token');
  serverInput = new FormControl('');
  @ViewChild('formDir') private formDir: NgForm;

  constructor(private fb:FormBuilder, private _FS:FetchService) { }

  ngOnInit(): void {
    this.serverForm = this.fb.group({
      server_name:['', Validators.required],
      ip: ['', Validators.required]
    });
  }
  createServer(){
    const formData = new FormData();
    formData.append('ip', this.serverForm.value.ip);
    formData.append('name', this.serverForm.value.server_name);
    if(this.serverForm.valid){
      this._FS.createServer(this.token, formData).subscribe(res=>{
        console.log(res, 'asdf')
        if(res.code===1){
          Swal.fire({
            icon: 'success',
            title: 'Successfully Submitted',   
            timer: 1500  
          });
          this.serverForm.reset();
        }
      })
    }
  }

}
