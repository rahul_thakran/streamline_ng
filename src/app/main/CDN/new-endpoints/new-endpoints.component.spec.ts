import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NewEndpointsComponent } from './new-endpoints.component';

describe('NewEndpointsComponent', () => {
  let component: NewEndpointsComponent;
  let fixture: ComponentFixture<NewEndpointsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NewEndpointsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NewEndpointsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
