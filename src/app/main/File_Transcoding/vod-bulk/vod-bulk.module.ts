import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { VodBulkRoutingModule } from './vod-bulk-routing.module';
import { VodBulkComponent } from './vod-bulk.component';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    VodBulkComponent
  ],
  imports: [
    CommonModule,
    VodBulkRoutingModule,
    NgMultiSelectDropDownModule.forRoot(),
    FormsModule,
    ReactiveFormsModule,
  ]
})
export class VodBulkModule { }
