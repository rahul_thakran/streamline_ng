import { ComponentFixture, TestBed } from '@angular/core/testing';

import { VodBulkComponent } from './vod-bulk.component';

describe('VodBulkComponent', () => {
  let component: VodBulkComponent;
  let fixture: ComponentFixture<VodBulkComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ VodBulkComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(VodBulkComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
