import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { VodBulkComponent } from './vod-bulk.component';

const routes: Routes = [
  {
    path: '', component: VodBulkComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class VodBulkRoutingModule { }
