import { Component, OnInit, ViewChild } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { FormBuilder, FormControl, FormGroup, NgForm, Validators } from '@angular/forms';
import { FetchService } from 'src/app/services/fetch.service';
import { ActivatedRoute, Params, Router } from '@angular/router';
import Swal from 'sweetalert2';
@Component({
  selector: 'app-add',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.scss']
})
export class AddComponent implements OnInit {
  token = localStorage.getItem('token');
  templateForm: FormGroup;
  cross;
  editFormData:any = {};
  paramId = '001XYZ100ABC';
  isTemplateEdit = false;
  @ViewChild("formDir") private formDir: NgForm;
  constructor(private http: HttpClient, private fb: FormBuilder, private router: Router, private fd: FetchService, private _ad:ActivatedRoute) { }

  ngOnInit(): void {
    this.templateForm = this.fb.group({
      name: ['', Validators.required],
      width: ['', Validators.required],
      height: ['', Validators.required],
      bitrate: ['', Validators.required],
      fps: ['', Validators.required],
      codec: ['', Validators.required],
      gop: ['', Validators.required],
      first: ['', Validators.required],
      second: ['', Validators.required],
      profile: ['', Validators.required],
      level: ['', Validators.required],
      audio_codec: ['', Validators.required],
      audio_bitrate: ['', Validators.required],
      sample_rate: ['', Validators.required],
      channel: ['', Validators.required],

    });
    this._ad.params.subscribe((params:Params)=>{
      let id = params['id'];
      if(id!=undefined){
        this.paramId = id;
        this.isTemplateEdit = true;
        this.onLoadData(params['id']);
      }
    });
  }
  onLoadData(id){
    return this.http.get(`https://goapi.multitvsolution.com:7003/streamlineapi/v1/get/template/token/${this.token}/id/${id}`).subscribe((res:any)=>{
      this.editFormData = res.result[0];
      let resolution:any = this.editFormData.video_resolution.split('X');
      let aspect_ratio:any = this.editFormData.aspect_ratio.split(':');
      this.templateForm.patchValue({
        name: this.editFormData.title,
        width: resolution[0],
        height: resolution[1],
        bitrate: this.editFormData.video_bitrate,
        fps: this.editFormData.video_fps,
        codec: this.editFormData.video_codec,
        gop: this.editFormData.video_gop,
        first: aspect_ratio[0],
        second: aspect_ratio[1],
        profile: this.editFormData.profile,
        level: this.editFormData.level,
        audio_codec: this.editFormData.audio_codec,
        audio_bitrate: this.editFormData.audio_bitrate,
        sample_rate: this.editFormData.audio_sample_rate,
        channel: this.editFormData.audio_channel  
      });
    })
  }
  onSubmit() {
    const formData = new FormData();
    if (this.templateForm.valid) {
      formData.append('title', this.templateForm.value.name);
      if(this.templateForm.value.width==""){
        this.cross=""
      }
      else{
        this.cross="X"
      }
      formData.append('video_resolution', this.templateForm.value.width + this.cross + this.templateForm.value.height);
      formData.append('video_bitrate', this.templateForm.value.bitrate);
      formData.append('video_fps', this.templateForm.value.fps);
      formData.append('video_codec', this.templateForm.value.codec);
      formData.append('video_gop', this.templateForm.value.gop);
      if(this.templateForm.value.first==""){
        this.cross=""
      }
      else{
        this.cross=":"
      }
      formData.append('aspect_ratio', this.templateForm.value.first + this.cross + this.templateForm.value.second);
      formData.append('profile', this.templateForm.value.profile);
      formData.append('level', this.templateForm.value.level);
      formData.append('audio_codec', this.templateForm.value.audio_codec);
      formData.append('audio_bitrate', this.templateForm.value.audio_bitrate);
      formData.append('audio_sample_rate', this.templateForm.value.sample_rate);
      formData.append('audio_channel', this.templateForm.value.channel);
      if(this.isTemplateEdit){
        formData.append('template_id', this.editFormData.template_id);
      }
      if(this.paramId === '001XYZ100ABC'){
        this.fd.submitTemplate(this.token, formData).subscribe((res: any) => {
          Swal.fire({
            icon: 'success',
            title: 'Successfully Submitted',      
          });
          this.router.navigate(['template-list']);
          this.templateForm.reset();
          this.formDir.resetForm();
        })
      }else{
        formData.append('id', this.paramId);
        this.fd.editTemplate(this.token, formData).subscribe((res: any) => {
          Swal.fire({
            icon: 'success',
            title: 'Successfully Submitted',      
          });
          this.router.navigate(['template-list']);
          this.templateForm.reset();
          this.formDir.resetForm();
        })
      }
      
    }
  }
}
