import { Component, OnInit } from '@angular/core';
import { FetchService } from 'src/app/services/fetch.service';
import { map } from 'rxjs/operators';
import Swal from 'sweetalert2';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent implements OnInit {
  token = localStorage.getItem('token');
  templateList: any = [];
  statuses: any = [];
  icon;
  active: boolean;
  inactive: boolean;
  rotate = true;
  maxSize = 5;
  status = "ON";
  offset=0;
  constructor(private fd: FetchService, private http:HttpClient) { }

  ngOnInit(): void {
    this.TemplateData(this.offset);
    this.active = false;
    this.inactive = false;
  }
  deleteList(list) {
    const formData = new FormData();
    formData.append('id', list.id);
    Swal.fire({
      title: 'Are you sure?',
      text: 'You want to delete the record!',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, change it!',
    }).then((result) => {
      if (result.isConfirmed) {
        this.fd.deleteTemplate(this.token, formData).subscribe(() => {
          Swal.fire('Deleted', 'Record Deleted Successfully', 'success');
          this.TemplateData(this.offset);
        })
      }
    });
  }

  TemplateData(offset) {
    this.fd.getTemplateDataFilter(this.token, offset).pipe(map(data => {
      this.templateList = data.result;
      this.statuses = data.result.map((item) => {
        return (
          item.status
        )
      })
      console.log(this.templateList);  
      for (let i in this.statuses) {
        if (this.statuses[i] == 1) {
          this.active = true
        }else {
          this.inactive = true
        }
      }
    })).subscribe();
  }
  toggleActive(list){
    Swal.fire({
      title: 'Are you sure?',
      text: 'You want to change the status!',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, change it!',
    }).then((result) => {
      if (result.isConfirmed) {
        const formData = new FormData();       
        formData.append('id', list.id);
        formData.append('field', 'status');
        formData.append('value', (list.status==1)?'0':'1');
        this.updateStatus(formData).subscribe((st:any)=>{
          if(st.code===1){
            Swal.fire('Changed', 'Status Changed Successfully', 'success');
            this.TemplateData(this.offset);
          }
        });
      }
    });
    
  }
  pageChanged(page:any){
    this.offset = (page.page*1 - 1)*10;
    this.TemplateData(this.offset);

  }
  updateStatus(params){
    return this.http.post(`https://goapi.multitvsolution.com:7003/streamlineapi/v1/template/update/data/token/${this.token}`, params);
  }
}
