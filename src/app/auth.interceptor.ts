import { HttpClient, HttpErrorResponse, HttpEvent, HttpHandler, HttpInterceptor, HttpParams, HttpRequest } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';

import { catchError, Observable, of, switchMap } from 'rxjs';
import { FetchService } from './services/fetch.service';



@Injectable()
export class AuthInterceptor implements HttpInterceptor {
  signature;
  constructor(private router: Router,private fd:FetchService, private http:HttpClient) { }
  refreshSignature(request, next){
    const formData = new FormData();
    const email = localStorage.getItem('email') as string
    const pwd=localStorage.getItem('pwd') as string
    formData.append('email', email);
    formData.append('pwd', pwd);
    this.fd.getTokens(formData).subscribe((res: any) => {
      if(res.code==1){
      this.signature=res.signature
      localStorage.setItem('signature', this.signature);
      return next.handle(request.clone({
        setHeaders:{
          Authorization:res.signature
        }
      }))
      }
    })
  }
  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    var signature = localStorage.getItem('signature');
    if(req.url.includes('streamlinego_epg')){
      req
    }else{
      req = req.clone({
        setHeaders: {
          'Authorization': `${signature}`,
        },
      });
    }
    return next.handle(req).pipe(
      catchError((err, caught: Observable<HttpEvent<any>>) => {
        if (err instanceof HttpErrorResponse && err.status == 401 && err.error.msg == "Invalid signature") {
          // this.refreshSignature(req, next);  
          const formData = new FormData();
          const email = localStorage.getItem('email') as string
          const pwd=localStorage.getItem('pwd') as string
          formData.append('email', email);
          formData.append('pwd', pwd);
          let params = new HttpParams();
          params = params.set('email', email);
          params = params.set('pwd', pwd);
            return this.http.post('https://goapi.multitvsolution.com:7003/streamlineapi/v1/auth/generate/token',params).pipe(switchMap((res:any)=>{
              this.signature=res.signature;
              localStorage.setItem('signature', this.signature);
              return next.handle(req.clone({
                setHeaders:{
                  Authorization:res.signature
                }
              }))
            }));    
        }
        throw err;
      })
    );
  }

}


// static accessToken = '';
//   refresh = false;

//   constructor(private http: HttpClient,private router: Router,private fd:FetchService) {
//   }

//   intercept(request: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {
//     const req = request.clone({
//       setHeaders: {
//         Authorization: `${AuthInterceptor.accessToken}`
//       }
//     });

//     return next.handle(req).pipe(catchError((err: HttpErrorResponse) => {
//       if (err.status === 401 && !this.refresh) {
//         this.refresh = true;
//         const formData = new FormData();
//         const email = localStorage.getItem('email') as string
//         const pwd=localStorage.getItem('pwd') as string
//         formData.append('email', email);
//         formData.append('pwd', pwd);
//         this.fd.getTokens(formData).pipe(switchMap((res:any)=>{
//           AuthInterceptor.accessToken=res.signature;
//           return next.handle(request.clone({
//             setHeaders:{
//               Authorization:res.signature
//             }
//           }))
//         })).subscribe();
        
//       }
//       this.refresh = false;
//       return throwError(() => err);
//     }));
//   }