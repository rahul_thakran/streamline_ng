import { Component, OnInit } from '@angular/core';
import { delay } from 'rxjs';
import { LoaderService } from './services/loader.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit{
  isLoaded: any = false;
  route;
  title = 'streamline';
  constructor(private _loading:LoaderService){}
  
  ngOnInit(): void {
    this.listenToLoading();
  }
  listenToLoading(): void {
    this._loading.loadingSub
      .pipe(delay(0)) // This prevents a ExpressionChangedAfterItHasBeenCheckedError for subsequent requests
      .subscribe((loading) => {
        this.isLoaded = loading;
      });
  }
}
