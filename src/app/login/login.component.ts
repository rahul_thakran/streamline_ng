


import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormControl, NgForm, Validators, FormBuilder } from '@angular/forms';
import { AuthService } from '../services/auth.service';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { Md5 } from 'ts-md5/dist/md5';
import { FetchService } from '../services/fetch.service';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  
  isInvalid = false;
  loginForm: FormGroup;
  invalidMsg = 'Invalid Email or Password';
  password;
  token;
  signature;
  
  @ViewChild('formDir') private formDir: NgForm;

  constructor(private fb: FormBuilder, private router: Router, private auth: AuthService, private ad: ActivatedRoute, private fd: FetchService) { }

  ngOnInit(): void {
  
    this.loginForm = this.fb.group({
      email: ['', Validators.required],
      password: ['', Validators.required],
    });

  }

 

  getToken() {
    let pass = this.loginForm.value.password;
    const md5 = new Md5();
    this.password = md5.appendStr(pass).end();
    const formData = new FormData();
    formData.append('email', this.loginForm.value.email);
    formData.append('pwd',this.password);
    this.fd.getTokens(formData).subscribe((res: any) => {
      if(res.code==1){
      this.token = res.token
      this.signature=res.signature
      localStorage.setItem('token', this.token)
      localStorage.setItem('signature', this.signature)
      if (this.loginForm.valid) {
        let pass = this.loginForm.value.password;
        const md5 = new Md5();
       this.password = md5.appendStr(pass).end();
       const token = localStorage.getItem('token')
        this.auth.loggedIn(this.loginForm.value.email, this.password,token).subscribe((res: any) => {
  
          if (res.code === 1) {
            localStorage.setItem('streamLineUser', JSON.stringify(res.result));
            localStorage.setItem('token', res.result.token);
            localStorage.setItem('email',res.result.email)
            localStorage.setItem('pwd',this.password)
            this.router.navigate(['dashboard']);
  
          } else {
            this.isInvalid = true;
  
          }
          this.loginForm.reset();
          this.formDir.resetForm();
        })
      }
      }
    })
    

  }



}