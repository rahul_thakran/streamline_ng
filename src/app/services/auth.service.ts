import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { environment } from '../../environments/environment';
@Injectable({
  providedIn: 'root'
})
export class AuthService {
  baseUrl = environment.baseUrl2;
  constructor(private http: HttpClient) { }


  
  loggedIn(email, password,token): Observable<any> {
    return this.http.get<any>(`${this.baseUrl}/streamlineapi/v1/auth/login/email/${email}/pwd/${password}/token/${token}`)
  }

  logout() {
    localStorage.removeItem('streamLineUser');
  }
}