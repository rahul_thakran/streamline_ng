import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { Observable } from 'rxjs';
var STREAM_BASE_URL =environment.streamBaseUrl;

@Injectable({
  providedIn: 'root',
})
export class FetchService {
  baseUrl = environment.baseUrl2;
  constructor(private http: HttpClient) {}

  submitTemplate(token, par) {
    return this.http.post<any>(
      `${this.baseUrl}/streamlineapi/v1/create/template/token/${token}`,
      par
    );
  }
  editTemplate(token, par) {
    return this.http.post<any>(
      `${this.baseUrl}/streamlineapi/v1/update/template/token/${token}`,
      par
    );
  }

  submitTranscoding(token, par) {
    return this.http.post<any>(
      `${this.baseUrl}/streamlineapi/v1/create/live/job/token/${token}`,
      par
    );
  }
  updateTranscoding(token, par) {
    return this.http.post<any>(
      `${this.baseUrl}/streamlineapi/v1/update/live/job/token/${token}`,
      par
    );
  }
  getTemplateData(token) {
    return this.http.get<any>(
      `${this.baseUrl}/streamlineapi/v1/get/template/token/${token}`
    );
  }
  getTemplateDataFilter(token, offset) {
    return this.http.get<any>(
      `${this.baseUrl}/streamlineapi/v1/get/template/token/${token}/offset/${offset}`
    );
  }
  searchList(names,token) {
    return this.http.get<any>(
      `${this.baseUrl}/streamlineapi/v1/get/live/job/token/${token}/search/${names}`
    );
  }
  getTranscoding(token, offset, filter) {
    return this.http.get<any>(
      `${this.baseUrl}/streamlineapi/v1/get/live/job/token/${token}/filter/${filter}/offset/${offset}`
    );
  }

  postStatus(token,par) {
    return this.http.post<any>(`${this.baseUrl}/streamlineapi/v1/job/update/data/token/${token}`, par);
  }

  deleteTemplate(token, par) {
    return this.http.post<any>(
      `${this.baseUrl}/streamlineapi/v1/template/delete/token/${token}`,
      par
    );
  }

  getTokens(par) {
    return this.http.post<any>(`${this.baseUrl}/streamlineapi/v1/auth/generate/token`,par);
  }

  getData(token,id){
    return this.http.get<any>(
      `${this.baseUrl}/streamlineapi/v1/get/live/job/token/${token}/job_id/${id}`
    )
  }
  getEpgChannels(todayDate:any): Observable<any> {
    // https://virtualapi.multitvsolution.com/upload_photo/streamlinego_epg.php?date=2022-02-09&token=dvkpk6239fdk
    // return this.http.get(`${this.baseUrl}/streamlineapi/v1/get/epg/program/token/dvkpk6239fdk/date/${todayDate}`);
    return this.http.get(`https://virtualapi.multitvsolution.com/upload_photo/streamlinego_epg.php?date=${todayDate}&token=dvkpk6239fdk`);
  }
  downloadEpg(epg):Observable<any>{
    return this.http.post(`${STREAM_BASE_URL}/RecognizerApi/index.php/api/Loockout/SaveToDownloads`, epg);
  }
  getChannels(token):Observable<any>{
    return this.http.get(`${this.baseUrl}/streamlineapi/v1/get/channel/token/${token}`);
  }

  getServerList(token){
    return this.http.get(`${this.baseUrl}/streamlineapi/v1/job/server/list/token/${token}`);
  }
  createServer(token, server):Observable<any>{
    return this.http.post(`${this.baseUrl}/streamlineapi/v1/job/server/add/token/${token}`, server);
  }
  deleteServer(token, server):Observable<any>{
    return this.http.post(`${this.baseUrl}/streamlineapi/v1/job/server/update/token/${token}`, server);
  }
  getTranscodingNotify(token, job_id){
    return this.http.get(`${this.baseUrl}/streamlineapi/v1/get/notification/token/${token}/job_id/${job_id}`);
  }
  updateNotifyStatus(token,param){
    return this.http.post(`${this.baseUrl}/streamlineapi/v1/update/status/notification/token/${token}`, param);
  }
}
