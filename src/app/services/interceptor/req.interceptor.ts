import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';

import {HttpInterceptor, HttpRequest, HttpHandler, HttpEvent, HttpResponse} from '@angular/common/http';

import {LoaderService} from '../../services/loader.service';
import { catchError, finalize, map } from "rxjs/operators";

@Injectable({
  providedIn: 'root'
})
export class ReqInterceptor implements HttpInterceptor  {

  constructor(private loader: LoaderService) {

  }

  intercept(req: HttpRequest<any>,next: HttpHandler): Observable<HttpEvent<any>> {
    // console.log('loading', req.url, this.handleRequestsAutomatically);

// If the Auto mode is turned off, do nothing
    // if (!this.handleRequestsAutomatically) {
    //     return next.handle(req);
    // }

    this.loader.setLoading(true, req.url);

    return next.handle(req).pipe(
        finalize(() => {
            console.log('Finished Loading', req.url);

            this.loader.setLoading(false, req.url);
        })
    );
}
}