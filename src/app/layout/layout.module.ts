import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NavbarComponent } from '../shared/navbar/navbar.component'

import { LayoutComponent } from './layout.component';
import { RouterModule } from '@angular/router';
import { LayoutRoutes } from './layout-routing.module';



@NgModule({
  declarations: [
 
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(LayoutRoutes)
    
  ]
})
export class LayoutModule { }
