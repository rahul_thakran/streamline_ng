import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [];

export const LayoutRoutes: Routes = [
  { path: 'dashboard', loadChildren: () => import('../main/CDN/dashboard/dashboard.module').then(m => m.DashboardModule) },
  { path: 'new-endpoints', loadChildren: () => import('../main/CDN/new-endpoints/new-endpoints.module').then(m => m.NewEndpointsModule) },
  { path: 'endpoints', loadChildren: () => import('../main/CDN/endpoints/endpoints.module').then(m => m.EndpointsModule) },  
  { path: 'vod', loadChildren: () => import('../main/File_Transcoding/vod/vod.module').then(m => m.VodModule) },
  { path: 'vod-bulk', loadChildren: () => import('../main/File_Transcoding/vod-bulk/vod-bulk.module').then(m => m.VodBulkModule) },
  { path: 'audio', loadChildren: () => import('../main/File_Transcoding/audio/audio.module').then(m => m.AudioModule) },  
  { path: 'add-template', loadChildren: () => import('../main/Template/add/add.module').then(m => m.AddModule) }, 
  { path: 'add-template/:id', loadChildren: () => import('../main/Template/add/add.module').then(m => m.AddModule) }, 
  { path: 'template-list', loadChildren: () => import('../main/Template/list/list.module').then(m => m.ListModule) },
  { path: 'add', loadChildren: () => import('../main/Live_Transcoding/add/add.module').then(m => m.AddModule) },
  { path: 'list', loadChildren: () => import('../main/Live_Transcoding/list/list.module').then(m => m.ListModule) },
  { path: 'settings', loadChildren: () => import('../main/settings/settings.module').then(m => m.SettingsModule) },
  { path: 'add/:id', loadChildren: () => import('../main/Live_Transcoding/add/add.module').then(m => m.AddModule) },
  { path:'epg', loadChildren:()=>import('../main/components/epg/epg.module').then(m=>m.EpgModule) },
  { path:'editor', loadChildren:()=>import('../main/components/video-editor/video-editor.module').then(m=>m.VideoEditorModule) },
  { path:'server', loadChildren:()=>import('../main/components/server/server.module').then(m=>m.ServerModule) },
];
