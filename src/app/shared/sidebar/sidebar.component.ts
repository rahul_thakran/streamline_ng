import { Component, OnInit } from '@angular/core';
import MetisMenu from 'metismenujs';
declare var $: any;

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',

})
export class SidebarComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
    new MetisMenu("#sidebar_menu");
   
  }



}
