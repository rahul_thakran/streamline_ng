import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { EventEmitter } from 'events';
import { AuthService } from '../../services/auth.service';
import * as $ from 'jquery';
@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',

})
export class NavbarComponent implements OnInit {
  user: any = JSON.parse(localStorage.getItem('streamLineUser') ?? '');


  constructor(public router: Router, private auth: AuthService) { }

  ngOnInit(): void {

  }

  logout() {
    this.auth.logout();
    this.router.navigate(['/login']);
  }





}
